import org.apache.spark._
import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.log4j.{Level, Logger}

object FindWords {
  def main(args: Array[String]) {
    val rootLogger = Logger.getLogger("org")
    rootLogger.setLevel(Level.ERROR)

    /*val logFile = "in.txt" // Should be some file on your system
    val spark = SparkSession.builder.appName("Simple Application").master("local[*]").getOrCreate()
    val input = spark.read.textFile(logFile).cache()*/

    val conf = new SparkConf().setAppName("SimpleApplication").setMaster("local[1]")
    val sc = new SparkContext(conf)
    val input = sc.textFile("in.txt")

    val numAs = input.filter(line => line.contains("a")).count()
    val numBs = input.filter(line => line.contains("b")).count()
    println(s"Lines with a: $numAs, Lines with b: $numBs")

    val count = input.flatMap(line => line.split("\\W+")).map(word ⇒ (word, 1))
      .reduceByKey(_ + _)
    count.saveAsTextFile("outfile")
    //spark.stop()
  }
}